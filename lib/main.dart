import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';

void main() => runApp(PianoApp());

class PianoApp extends StatelessWidget {
  void playSound(String noteSound) {
    final player = AudioPlayer();
    player.setAsset('./assets/note_sound/$noteSound.mp3');
    player.play();
  }

  Flexible pianoKey({String noteSound, String noteImage}) {
    return Flexible(
      child: TextButton(
        child: Image.asset('assets/note_image/$noteImage.png'),
        onPressed: () {
          playSound(noteSound);
          print(noteSound);
        },
      ),
    
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              pianoKey(noteSound: 'C3', noteImage: 'do'),
              pianoKey(noteSound: 'D3', noteImage: 're'),
              pianoKey(noteSound: 'E3', noteImage: 'mi'),
              pianoKey(noteSound: 'F3', noteImage: 'fa'),
              pianoKey(noteSound: 'G3', noteImage: 'sol'),
              pianoKey(noteSound: 'A3', noteImage: 'la'),
              pianoKey(noteSound: 'B3', noteImage: 'si'),
              pianoKey(noteSound: 'C4', noteImage: 'do'),
            ],
          ),
        ),
      ),
    );
  }
}
